﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SeaFoodApi2.Model.Models;

namespace SeaFoodApi2.Controllers
{
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {

        // GET: api/Login
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        
        //private IActionResult BuildToken(userToken userInfo)
        //{
        //    var claims = new[]
        //}
        // POST: api/Login
        [HttpPost]
        public IActionResult Post([FromBody]string loginModel)
        {
            
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("[Clave]"));
            JwtSecurityToken token = new JwtSecurityToken(
                    //claims: Claim,
                    issuer: "issuer",
                    audience: "audience",
                    expires: DateTime.Now.AddDays(30),
                    signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
                );

            return new OkObjectResult(new { Token = new JwtSecurityTokenHandler().WriteToken(token) });

        }

        // PUT: api/Login/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
