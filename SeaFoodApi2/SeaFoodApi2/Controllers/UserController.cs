﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using SeaFoodApi2.Model.Models;
using SeaFoodApi2.Resources;
using SeaFoodApi2.Service.Services;

namespace SeaFoodApi2.Controllers
{
    [Route("api/[controller]")]
   
 
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            this._userService = userService;

        }
        // GET: api/User1
        [HttpGet]
        public async Task<IActionResult> GetAllUser()
        {
            try
            {
                //var Users = _userService.GetUsers();
                //return Ok(Users);

                return Ok(await _userService.GetUsers());
            }
            catch (Exception)
            {

                return BadRequest(Messages.UserGetError);
            }
        }
        // GET: api/User1/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                //var userFinded = _userService.GetId(id);

                return Ok(await _userService.GetId(id));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        // POST: api/User1
        [HttpPost]
        public  async Task<IActionResult> PostUser([FromBody] User user)
        {
            try
            {
                //if (ModelState.IsValid)
                //{
                   // _userService.AddUser(user);
                    return Ok(await _userService.AddUser(user));
                ///}


            }
            catch (Exception)
            {
                return BadRequest();
            }


        }


    }
}