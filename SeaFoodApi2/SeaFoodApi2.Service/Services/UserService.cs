﻿using SeaFoodApi2.Model.Models;
using SeaFoodApi2.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SeaFoodApi2.Service.Services
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetId(int id);
        Task<User>  AddUser(User user);
       
    }
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<IEnumerable<User>> GetUsers()
        {
            var user = await _userRepository.GetAll();
            return (user);
        }

        public async Task<User> GetId(int id)
        {
            //return _userRepository.GetId(id);
            var user = await _userRepository.GetId(id);
            return (user);
        }

        public async Task<User> AddUser(User user)
        {
            //_userRepository.Insert(user);
            await _userRepository.Insert(user);
            return user;
        }

    }
}
