﻿using Microsoft.EntityFrameworkCore;
using SeaFoodApi2.Model.Models;
using SeaFoodApi2.Repository.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SeaFoodApi2.Repository.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> GetAll();
        Task<User> GetId(int id);
        Task<User> Insert(User user);

    }
    public class UserRepository: IUserRepository
    {
        private readonly DataDbContext _dbContext = new DataDbContext();
        public async Task<User> GetId(int id)
        {
            {
                //return _dbContext.User.Find(id);
                var user = await _dbContext.User.FindAsync(id);
                return (user);
            }

        }

        public async Task<IEnumerable<User>> GetAll()
        {
            var user = await _dbContext.User.ToArrayAsync();
            return (user);

        }

        public async Task<User> Insert(User user)
        {
            _dbContext.User.Add(user);
            await _dbContext.SaveChangesAsync();
            return user;

        }
    }
}
