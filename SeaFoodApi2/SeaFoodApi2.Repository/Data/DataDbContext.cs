﻿using Microsoft.EntityFrameworkCore;
using SeaFoodApi2.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeaFoodApi2.Repository.Data
{
    public class DataDbContext: DbContext
    {
        public DataDbContext()
        {

        }

        public DataDbContext(DbContextOptions options)
        : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //En caso de que el contexto no este configurado, lo configuramos mediante la cadena de conexion
            if (!optionsBuilder.IsConfigured)
            {
                _ = optionsBuilder.UseSqlServer("Server = .;Database=Seafood; Uid=sa; Pwd=uts;");
            }
        }

        public virtual DbSet<User> User { get; set; }
    }
}
